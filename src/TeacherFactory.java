import java.util.Scanner;


public class TeacherFactory extends PersonFactory implements Factory<Teacher> {

	public TeacherFactory(Scanner scan) {
		super(scan);
	}

	@Override
	public Teacher create() {
		
		System.out.print("Vad kallas l�raren?: ");
		//String name = currentScanner().nextLine();
		
		Teacher newTeacher = new Teacher("sebastian", Subject.JAVA, Subject.PYTHON);
		
		return newTeacher;
	}

}
