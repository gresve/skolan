
public class Address implements java.io.Serializable {

	private transient static Address defaultAddress = null;
	
	private String street;
	private String postCode;
	
	private Address() {
		this("DEFAULT", "000 00");
	}
	
	public Address(String street, String postCode) {
		this.street = street;
		this.postCode = postCode;
	}
	
	public String get() {
		return street + " - " + postCode;
	}
	
	public static Address getDefault() {
		if (defaultAddress == null) {
			defaultAddress = new Address();
		}
		
		return defaultAddress;
	}
	
}
