
public class Teacher extends Person {
	
	private Subject[] skillz;
	
	public Teacher(String name, Subject... skillz) {
		super(name, 1973);
		
		this.skillz = skillz;
	}
	
	@Override
	public String toString() {
		return "Teacher: " + super.toString();
	}
}
