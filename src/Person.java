
public abstract class Person implements java.io.Serializable {
	
	private Address address;
	private String name;
	private int birthyear;
	
	public Person(String name, int birthyear) {
		this(name, birthyear, Address.getDefault());
	}

	public Person(String name, int birthyear, Address address) {
		this.address = address;
		this.name = name;
		this.birthyear = birthyear;
	}
	
	@Override
	public String toString() {
		return name + ", year: " + birthyear + ". address: " + address.get();
	}
	
}
