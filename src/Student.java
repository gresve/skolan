
public class Student extends Person {

	private float avgGrades;
	
	public Student(String name, int birthyear) {
		super(name, birthyear);
		
		this.avgGrades = 5;
	}

	@Override
	public String toString() {
		return "Student: " + super.toString() + ", grades: " + avgGrades;
	}
}
