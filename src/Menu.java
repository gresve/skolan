import java.util.Scanner;


public class Menu {

	private Scanner scanner;
	private Skola skolan;
	private StudentFactory studentFact;
	private TeacherFactory teacherFact;
	
	public Menu() {
		scanner = new Scanner(System.in);
		
		skolan = new Skola();
		studentFact = new StudentFactory(scanner);
		teacherFact = new TeacherFactory(scanner);
	}
	
	public void runMenu() {
		boolean run = false;
		
		do{
			int input = printChoises(scanner);
			run = true;
			
			switch(input){
			case 1:
				createPerson(studentFact);
				break;
			case 2:
				createPerson(teacherFact);
				break;
			case 3:
				skolan.printStudents();
				break;
			case 4:
				skolan.printTeachers();
				break;
			default:
				run = false;
			}
			
		}while(run);
		
		skolan.save();
	}
	
	private <T extends Person>void createPerson(Factory<T> factory) {
		Person person = factory.create();
		skolan.addPerson(person);
	}
	
	private int printChoises(Scanner scanner) {
		System.out.println("Hej všlkommen");
		System.out.println("1: Add student");
		System.out.println("2: Add teacher");
		System.out.println("3: Print student");
		System.out.println("4: Print teachers");
		
		int input = scanner.nextInt();
		return input;
	}
}
