import java.io.*;
import java.util.ArrayList;


public class Skola {
	
	private ArrayList<Teacher> teachers;
	private ArrayList<Student> students;
	private ArrayList<Program> programs;
	
	public Skola() {
		load();
	}
	
	public void addPerson(Person person) {
		if (person instanceof Student) {
			students.add((Student)person);
		}else {
			teachers.add((Teacher)person);
		}
	}
	
	public void printStudents() {
		printPeople(students);
	}
	
	public void printTeachers() {
		printPeople(teachers);
	}
	
	private <T extends Person>void printPeople(ArrayList<T> people) {
		for(Person person : people) {
			System.out.println(person.toString());
		}
	}
	
	public void addStudentToProgram(Student student, Program program) {
		// TODO MAKE FIX
	}
	
	public void load() {
		students = loadPeople("students.dat");
		teachers = loadPeople("teachers.dat");
		
		if(students == null) {
			students = new ArrayList<Student>();
		}
		
		if(teachers == null) {
			teachers = new ArrayList<Teacher>();
		}
		
		programs = new ArrayList<Program>();
	}
	
	private <T extends Person>ArrayList<T> loadPeople(String path){
		ArrayList<T> result = null;
		
		try{
			FileInputStream inputStream = new FileInputStream(path);
			ObjectInputStream objectStream = new ObjectInputStream(inputStream);
			result = (ArrayList<T>)objectStream.readObject();
			objectStream.close();
			inputStream.close();
		}catch(Exception e){
			System.out.println("-OJoj saker �r trasigt i load-");
		}
		
		return result;
	} 
	
	public void save() {
		savePeople(students, "students.dat");
		savePeople(teachers, "teachers.dat");
	}
	
	private <T extends java.io.Serializable>void savePeople(T people, String path) {
		try{
			FileOutputStream outputStream = new FileOutputStream(path);
			ObjectOutputStream stream = new ObjectOutputStream(outputStream);
			stream.writeObject(people);
			stream.close();
			outputStream.close();
		}catch(Exception e){
			System.out.println("-Save failed-");
		}
	}
}



