import java.util.Scanner;


public abstract class PersonFactory {
	
	private Scanner scanner;
	
	public PersonFactory(Scanner scan) {
		this.scanner = scan;
	}

	protected Scanner currentScanner() {
		return scanner;
	}
}
